package com.mallik.chat;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;
import org.skyscreamer.jsonassert.JSONAssert;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
@RunWith(RobolectricTestRunner.class)
public class ChatPresenterTest {


    ChatPresenter subject;

    @Mock
    ChatPresenter.ChatPresenterInterface mockView;

    @Before
    public void setUp(){
        initMocks(this);
        subject = new ChatPresenter(mockView);
    }


    @Test
    public void subject_should_handle_emptymessage() throws JSONException {
        subject.extractAll("");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_mention() throws JSONException {
        subject.extractAll("@chris");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\"mentions\":[\"chris\"]}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_mention_with_underscore() throws JSONException {
        subject.extractAll("@chris_chirs");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\"mentions\":[\"chris_chirs\"]}";
        JSONAssert.assertEquals(expected, actual, true);
    }


    @Test
    public void subject_should_parse_all_mentions_with_number() throws JSONException {
        subject.extractAll("@123");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\"mentions\":[\"123\"]}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_all_mentions_with_dot() throws JSONException {
        subject.extractAll("@12.3");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\"mentions\":[\"12\"]}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_message_with_random_characters() throws JSONException {
        subject.extractAll("@abc-)!@#$%^&*(");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\"mentions\":[\"abc\"]}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_message_with_no_word() throws JSONException {
        subject.extractAll("@");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_single_emoticons() throws JSONException {
        subject.extractAll("(megusta)");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\"emoticons\":[\"megusta\"]}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_single_emoticons_greater_than_15_characters() throws JSONException {
        subject.extractAll("(thisisawesomeassignment)");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_empty_emoticons() throws JSONException {
        subject.extractAll("()");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_ignore_emoticons_with_space() throws JSONException {
        subject.extractAll("(abc xyz)");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_ignore_emoticons_with_special_characters() throws JSONException {
        subject.extractAll("(abc!@#$%^&*())");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_emoticons_with_number() throws JSONException {
        subject.extractAll("(12345)");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\"emoticons\":[\"12345\"]}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_all_emoticons() throws JSONException {
        subject.extractAll("Good morning! (megusta) (coffee)");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\"emoticons\":[\"megusta\",\"coffee\"]}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_mention_and_emoticon() throws JSONException {
        subject.extractAll("Good morning! @mallik (coffee)");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\n" +
                "  \"mentions\": [\n" +
                "    \"mallik\"\n" +
                "  ],\n" +
                "  \"emoticons\": [\n" +
                "    \"coffee\"\n" +
                "  ]\n" +
                "}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_all_mention_and_all_emoticon() throws JSONException {
        subject.extractAll("Good morning! (smile) @mallik (coffee) @here");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\n" +
                "  \"mentions\": [\n" +
                "    \"mallik\",\n" +
                "    \"here\"\n" +
                "  ],\n" +
                "  \"emoticons\": [\n" +
                "    \"smile\",\n" +
                "    \"coffee\"\n" +
                "  ]\n" +
                "}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_single_url() throws JSONException {
        subject.extractAll("http://www.google.com");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\n" +
                "  \"links\": [\n" +
                "    {\n" +
                "      \"url\": \"http://www.google.com\",\n" +
                "      \"title\": \"Url header goes here\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_all_protocols_in_url() throws JSONException {
        subject.extractAll("https://www.google.com");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\n" +
                "  \"links\": [\n" +
                "    {\n" +
                "      \"url\": \"https://www.google.com\",\n" +
                "      \"title\": \"Url header goes here\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        JSONAssert.assertEquals(expected, actual, true);
    }


    @Test
    public void subject_should_parse_all_url_extensions() throws JSONException {
        subject.extractAll("http://usa.gov");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\n" +
                "  \"links\": [\n" +
                "    {\n" +
                "      \"url\": \"http://usa.gov\",\n" +
                "      \"title\": \"Url header goes here\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_ignore_invalid_space_protocol() throws JSONException {
        subject.extractAll("ht tp://www.nbcolympics.com");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_ignore_invalid_special_char_protocol() throws JSONException {
        subject.extractAll("ht#tp://www.nbcolympics.com");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_url_with_space() throws JSONException {
        subject.extractAll("http://www.nbcoly abc.com");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\n" +
                "  \"links\": [\n" +
                "    {\n" +
                "      \"title\": \"Url header goes here\",\n" +
                "      \"url\": \"http:\\/\\/www.nbcoly\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_long_url() throws JSONException {
        subject.extractAll("https://example.org/dialog?embedded=true#%7B%22hostlib%22%3A%22https%3A%2F%2Fwww.hipchat.com%2Fmin%2F%3Fb%3Djs%26f%3Dconnect%2Fall-debug.js%2Cconnect%2Fplugin-init.js%22%2C%22capabilities%22%3A%7B%22nativeApi%22%3A%22acPostMessage%22%2C%22resize%22%3A%7B%7D%2C%22close%22%3A%7B%7D%7D%2C%22height%22%3A%22400px%22%2C%22width%22%3A%22300px%22%2C%22resize%22%3Atrue%7D");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\n" +
                "  \"links\": [\n" +
                "    {\n" +
                "      \"title\": \"Url header goes here\",\n" +
                "      \"url\": \"https:\\/\\/example.org\\/dialog?embedded=true#%7B%22hostlib%22%3A%22https%3A%2F%2Fwww.hipchat.com%2Fmin%2F%3Fb%3Djs%26f%3Dconnect%2Fall-debug.js%2Cconnect%2Fplugin-init.js%22%2C%22capabilities%22%3A%7B%22nativeApi%22%3A%22acPostMessage%22%2C%22resize%22%3A%7B%7D%2C%22close%22%3A%7B%7D%7D%2C%22height%22%3A%22400px%22%2C%22width%22%3A%22300px%22%2C%22resize%22%3Atrue%7D\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_mention_emoticon_url() throws JSONException {
        subject.extractAll("Good morning! @mallik (coffee) http://www.starbucks.com");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\n" +
                "  \"links\": [\n" +
                "    {\n" +
                "      \"title\": \"Url header goes here\",\n" +
                "      \"url\": \"http:\\/\\/www.starbucks.com\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"mentions\": [\n" +
                "    \"mallik\"\n" +
                "  ],\n" +
                "  \"emoticons\": [\n" +
                "    \"coffee\"\n" +
                "  ]\n" +
                "}";
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void subject_should_parse_mention_emoticon_url_even_without_spaces() throws JSONException {
        subject.extractAll("@mallik(coffee)http://www.starbucks.com");
        ArgumentCaptor<String> outArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockView).populateMentions(outArgumentCaptor.capture());
        String actual = outArgumentCaptor.getValue();
        String expected = "{\n" +
                "  \"links\": [\n" +
                "    {\n" +
                "      \"title\": \"Url header goes here\",\n" +
                "      \"url\": \"http:\\/\\/www.starbucks.com\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"mentions\": [\n" +
                "    \"mallik\"\n" +
                "  ],\n" +
                "  \"emoticons\": [\n" +
                "    \"coffee\"\n" +
                "  ]\n" +
                "}";
        JSONAssert.assertEquals(expected, actual, true);
    }


}