package com.mallik.chat;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class ChatActivity extends Activity implements ChatPresenter.ChatPresenterInterface{

    TextView outputText;
    EditText messageEditText;
    TextView submitButton;

    ChatPresenter chatPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        chatPresenter = new ChatPresenter(this);

        outputText = (TextView)findViewById(R.id.outputId);
        messageEditText = (EditText)findViewById(R.id.messageBoxId);
        submitButton =(TextView)findViewById(R.id.submitId);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboardOnSend(v);
                sendMessage();
            }
        });

        messageEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    sendMessage();
                    handled = true;
                }

                hideKeyboardOnSend(v);
                return handled;
            }
        });
    }

    private void hideKeyboardOnSend(View v) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(),InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    private void sendMessage() {
        chatPresenter.extractAll(messageEditText.getText().toString());
    }


    @Override
    public void populateMentions(String mentionJsonOutput) {
        outputText.setText(mentionJsonOutput);
        messageEditText.getText().clear();
    }
}
