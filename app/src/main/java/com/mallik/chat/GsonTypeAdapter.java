package com.mallik.chat;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.List;

public class GsonTypeAdapter extends TypeAdapter<OutputJson>{


    public static final String MENTIONS = "mentions";
    public static final String EMOTICONS = "emoticons";
    public static final String LINKS = "links";

    @Override
    public void write(JsonWriter out, OutputJson outputJson) throws IOException {
        out.beginObject();

        if(outputJson.getMentions().size() > 0){
            out.name(MENTIONS);
            writeList(out, outputJson.getMentions());
        }


        if(outputJson.getEmoticons().size() > 0){
            out.name(EMOTICONS);
            writeList(out, outputJson.getEmoticons());
        }

        if(outputJson.getLinks().size() > 0) {
            out.name(LINKS);
            writeLinksList(out, outputJson.getLinks());
        }


        out.endObject();

    }

    @Override
    public OutputJson read(JsonReader in) throws IOException {
        return null;
    }

    public static  void writeList(JsonWriter writer, List<String> strings) throws IOException {
        writer.beginArray();
        for (String value : strings) {
            writer.value(value);
        }
        writer.endArray();
    }

    public static  void writeLinksList(JsonWriter writer, List<String> strings) throws IOException {
        writer.beginArray();
        for (String value : strings) {
                writer.beginObject();
                writer.name("url");
                writer.value(value);
                writer.name("title");
                writer.value("Url header goes here");
                writer.endObject();
        }
        writer.endArray();
    }
}
