package com.mallik.chat;

import java.util.ArrayList;

public class OutputJson {

    private ArrayList<String> mentions;
    private ArrayList<String> emoticons;
    private ArrayList<String> links;

    public ArrayList<String> getMentions() {
        return mentions;
    }

    public void setMentions(ArrayList<String> mentions) {
        this.mentions = mentions;
    }

    public ArrayList<String> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(ArrayList<String> emoticons) {
        this.emoticons = emoticons;
    }

    public ArrayList<String> getLinks() {
        return links;
    }

    public void setLinks(ArrayList<String> links) {
        this.links = links;
    }
}
