package com.mallik.chat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatPresenter {


    public static final int MENTIONS = 1;
    public static final int EMOTICONS = 2;
    public static final int URLS = 3;
    ChatPresenterInterface view;

    public ChatPresenter(ChatPresenterInterface view) {
        this.view = view;
    }

    public void extractAll(String chatMessage){
        ArrayList<String> mentions = new ArrayList<>();
        ArrayList<String> emoticons = new ArrayList<>();
        ArrayList<String> urls = new ArrayList<>();
        populateListData(chatMessage, mentions, emoticons, urls);
        OutputJson jsonOutputJson = buildOutputJson(mentions, emoticons, urls);
        String finalOutputJson = convertListToJsonString(jsonOutputJson);
        updateViewWithData(finalOutputJson);
    }

    private void populateListData(String chatMessage, ArrayList<String> mentions, ArrayList<String> emoticons, ArrayList<String> urls) {
        Pattern p = Pattern.compile("(@\\w+)|(\\(\\w+\\))|(http(s|)://[^\\s]+(\\s|))");
        Matcher m = p.matcher(chatMessage);
        while (m.find()) {
            for(int i=1;i<=m.groupCount();i++) {
                if (m.group(i) != null) {
                    populateData(mentions, emoticons, urls, m, i);
                }
            }
        }
    }

    private void updateViewWithData(String finalOutputJson) {
        try {
            String jsonFormat = convertToHumanReadableFormat(finalOutputJson);
            view.populateMentions(jsonFormat);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String convertListToJsonString(OutputJson jsonOutputJson) {
        Gson gson = new GsonBuilder().registerTypeAdapter(OutputJson.class,new GsonTypeAdapter()).create();
        return gson.toJson(jsonOutputJson);
    }

    private String convertToHumanReadableFormat(String finalOutputJson) throws JSONException {
        String jsonFormat;
        jsonFormat = (new JSONObject(finalOutputJson)).toString(2).replace("\\","");
        return jsonFormat;
    }

    private OutputJson buildOutputJson(ArrayList<String> mentions, ArrayList<String> emoticons, ArrayList<String> urls) {
        OutputJson jsonOutputJson = new OutputJson();
        jsonOutputJson.setMentions(mentions);
        jsonOutputJson.setEmoticons(emoticons);
        jsonOutputJson.setLinks(urls);
        return jsonOutputJson;
    }

    private void populateData(ArrayList<String> mentions, ArrayList<String> emoticons, ArrayList<String> urls, Matcher m, int i) {
        switch (i) {
            case MENTIONS:
                mentions.add(m.group(MENTIONS).replace("@",""));
                break;
            case EMOTICONS:
                addEmoticons(emoticons, m);
                break;
            case URLS:
                urls.add(m.group(URLS).trim());
                break;
        }
    }

    private void addEmoticons(ArrayList<String> emoticons, Matcher m) {
        String emiticonsValue = m.group(EMOTICONS);
        if(emiticonsValue.length() <= 15) {
            emoticons.add(emiticonsValue.replaceAll("[()]", "").trim());
        }
    }

    public interface ChatPresenterInterface{
        void  populateMentions(String mentionJsonOutput);
    }

}
